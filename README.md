etherdump
=========

Tool to publish [etherpad](http://etherpad.org/) pages to files.


Requirements
-------------
	* python3
	* html5lib
	* requests (settext)
	* python-dateutil, jinja2 (index subcommand)

Installation
-------------

    pip install python-dateutil jinja2 html5lib
    python setup.py install

Example
---------------
	mkdir mydump
	cd myddump
	etherdump init

The program then interactively asks some questions:

	Please type the URL of the etherpad: 
		http://automatist.local:9001/
	The APIKEY is the contents of the file APIKEY.txt in the etherpad folder
	Please paste the APIKEY: 
		xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

The settings are placed in a file called .etherdump/settings.json and are used (by default) by future commands.



subcommands
----------

* init
* pull
* list
* listauthors
* gettext
* settext
* gethtml
* creatediffhtml
* revisionscount
* index
* deletepad

To get help on a subcommand:

	etherdump revisionscount --help


Change log / notes
=======================

Originally designed for use at: [constant](http://etherdump.constantvzw.org/).


17 Oct 2016
-----------------------------------------------
Preparations for [Machine Research](https://machineresearch.wordpress.com/) [2](http://constantvzw.org/site/Machine-Research,2646.html)


6 Oct 2017
----------------------
Feature request from PW: When deleting a previously public document, generate a page / pages with an explanation (along the lines of "This document was previously public but has been marked .... maybe give links to search").

3 Nov 2017
---------------
machineresearch seems to be __NOPUBLISH__ but still exists (also in recentchanges)

Jan 2018
-------------
Updated files to work with python3 (probably this has broken python2).


